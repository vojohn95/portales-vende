<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateDatoRequest;
use App\Http\Requests\UpdateDatoRequest;
use App\Repositories\DatoRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class DatoController extends AppBaseController
{
    /** @var  DatoRepository */
    private $datoRepository;

    public function __construct(DatoRepository $datoRepo)
    {
        $this->datoRepository = $datoRepo;
    }

    /**
     * Display a listing of the Dato.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $datos = $this->datoRepository->all();

        return view('datos.index')
            ->with('datos', $datos);
    }

    /**
     * Show the form for creating a new Dato.
     *
     * @return Response
     */
    public function create()
    {
        return view('datos.create');
    }

    /**
     * Store a newly created Dato in storage.
     *
     * @param CreateDatoRequest $request
     *
     * @return Response
     */
    public function store(CreateDatoRequest $request)
    {
        $input = $request->all();

        $dato = $this->datoRepository->create($input);

        Flash::success('Dato saved successfully.');

        return redirect(route('datos.index'));
    }

    /**
     * Display the specified Dato.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $dato = $this->datoRepository->find($id);

        if (empty($dato)) {
            Flash::error('Dato not found');

            return redirect(route('datos.index'));
        }

        return view('datos.show')->with('dato', $dato);
    }

    /**
     * Show the form for editing the specified Dato.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $dato = $this->datoRepository->find($id);

        if (empty($dato)) {
            Flash::error('Dato not found');

            return redirect(route('datos.index'));
        }

        return view('datos.edit')->with('dato', $dato);
    }

    /**
     * Update the specified Dato in storage.
     *
     * @param int $id
     * @param UpdateDatoRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateDatoRequest $request)
    {
        $dato = $this->datoRepository->find($id);

        if (empty($dato)) {
            Flash::error('Dato not found');

            return redirect(route('datos.index'));
        }

        $dato = $this->datoRepository->update($request->all(), $id);

        Flash::success('Dato updated successfully.');

        return redirect(route('datos.index'));
    }

    /**
     * Remove the specified Dato from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $dato = $this->datoRepository->find($id);

        if (empty($dato)) {
            Flash::error('Dato not found');

            return redirect(route('datos.index'));
        }

        $this->datoRepository->delete($id);

        Flash::success('Dato deleted successfully.');

        return redirect(route('datos.index'));
    }
}
