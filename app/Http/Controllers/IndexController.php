<?php

namespace App\Http\Controllers;

use App\Http\Controllers\AppBaseController;
use App\Http\Requests\CreateDatoRequest;
use App\Http\Requests\UpdateDatoRequest;
use App\Repositories\DatoRepository;
use Illuminate\Http\Request;
use Flash;
use Response;


class IndexController extends Controller
{

    private $datoRepository;

    public function __construct(DatoRepository $datoRepo)
    {
        $this->datoRepository = $datoRepo;
    }

    public function index()
    {
        $datos = $this->datoRepository->all();

        return view('welcome')
            ->with('datos', $datos);
    }

    public function AjaxRequestNuevo(Request $request)
    {
        //$input = $request->all();

        $res = Flash::success('Informacion añadida con exito');;
        return response()->json($res, 200);
    }

    public function AñadirNuevo()
    {
        $input = request()->all();
        $dato = $this->datoRepository->create($input);
        $datos = $this->datoRepository->all();

        Flash::success('<h5 align="center">¡Bienvenido al grupo!.</h5>');

        return view('welcome')->with('datos',$datos);
    }
}
