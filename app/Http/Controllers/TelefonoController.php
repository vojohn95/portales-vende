<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateTelefonoRequest;
use App\Http\Requests\UpdateTelefonoRequest;
use App\Repositories\TelefonoRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;

class TelefonoController extends AppBaseController
{
    /** @var  TelefonoRepository */
    private $telefonoRepository;

    public function __construct(TelefonoRepository $telefonoRepo)
    {
        $this->telefonoRepository = $telefonoRepo;
    }

    /**
     * Display a listing of the Telefono.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $telefonos = $this->telefonoRepository->all();

        return view('telefonos.index')
            ->with('telefonos', $telefonos);
    }

    /**
     * Show the form for creating a new Telefono.
     *
     * @return Response
     */
    public function create()
    {
        return view('telefonos.create');
    }

    /**
     * Store a newly created Telefono in storage.
     *
     * @param CreateTelefonoRequest $request
     *
     * @return Response
     */
    public function store(CreateTelefonoRequest $request)
    {
        $input = $request->all();

        $telefono = $this->telefonoRepository->create($input);

        Flash::success('Telefono saved successfully.');

        return redirect(route('telefonos.index'));
    }

    /**
     * Display the specified Telefono.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $telefono = $this->telefonoRepository->find($id);

        if (empty($telefono)) {
            Flash::error('Telefono not found');

            return redirect(route('telefonos.index'));
        }

        return view('telefonos.show')->with('telefono', $telefono);
    }

    /**
     * Show the form for editing the specified Telefono.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $telefono = $this->telefonoRepository->find($id);

        if (empty($telefono)) {
            Flash::error('Telefono not found');

            return redirect(route('telefonos.index'));
        }

        return view('telefonos.edit')->with('telefono', $telefono);
    }

    /**
     * Update the specified Telefono in storage.
     *
     * @param int $id
     * @param UpdateTelefonoRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateTelefonoRequest $request)
    {
        $telefono = $this->telefonoRepository->find($id);

        if (empty($telefono)) {
            Flash::error('Telefono not found');

            return redirect(route('telefonos.index'));
        }

        $telefono = $this->telefonoRepository->update($request->all(), $id);

        Flash::success('Telefono updated successfully.');

        return redirect(route('telefonos.index'));
    }

    /**
     * Remove the specified Telefono from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        $telefono = $this->telefonoRepository->find($id);

        if (empty($telefono)) {
            Flash::error('Telefono not found');

            return redirect(route('telefonos.index'));
        }

        $this->telefonoRepository->delete($id);

        Flash::success('Telefono deleted successfully.');

        return redirect(route('telefonos.index'));
    }
}
