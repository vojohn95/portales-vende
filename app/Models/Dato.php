<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Dato
 * @package App\Models
 * @version May 14, 2020, 6:08 am UTC
 *
 * @property \Illuminate\Database\Eloquent\Collection $telefonos
 * @property string $nombre
 * @property string $telefono
 * @property string $productos
 * @property string $horario
 * @property string $imagen
 */
class Dato extends Model
{
    use SoftDeletes;

    public $table = 'data';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'nombre',
        'telefono',
        'productos',
        'horario',
        'imagen'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'nombre' => 'string',
        'telefono' => 'string',
        'productos' => 'string',
        'horario' => 'string',
        'imagen' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'nombre' => 'required',
        'telefono' => 'required',
        'productos' => 'required'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function telefonos()
    {
        return $this->hasMany(\App\Models\Telefono::class, 'id_data');
    }
}
