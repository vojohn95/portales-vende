<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Telefono
 * @package App\Models
 * @version May 14, 2020, 6:03 am UTC
 *
 * @property \App\Models\Data $idData
 * @property integer $id_data
 * @property string $telefono
 */
class Telefono extends Model
{
    use SoftDeletes;

    public $table = 'telefonos';
    
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';


    protected $dates = ['deleted_at'];



    public $fillable = [
        'id_data',
        'telefono'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'id_data' => 'integer',
        'telefono' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'id_data' => 'required',
        'telefono' => 'required'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function idData()
    {
        return $this->belongsTo(\App\Models\Data::class, 'id_data');
    }
}
