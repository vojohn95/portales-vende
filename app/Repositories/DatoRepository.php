<?php

namespace App\Repositories;

use App\Models\Dato;
use App\Repositories\BaseRepository;

/**
 * Class DatoRepository
 * @package App\Repositories
 * @version May 14, 2020, 6:08 am UTC
*/

class DatoRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'nombre',
        'telefono',
        'productos',
        'horario',
        'imagen'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Dato::class;
    }
}
