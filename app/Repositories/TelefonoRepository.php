<?php

namespace App\Repositories;

use App\Models\Telefono;
use App\Repositories\BaseRepository;

/**
 * Class TelefonoRepository
 * @package App\Repositories
 * @version May 14, 2020, 6:03 am UTC
*/

class TelefonoRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'id_data',
        'telefono'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Telefono::class;
    }
}
