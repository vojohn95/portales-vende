<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Dato;
use Faker\Generator as Faker;

$factory->define(Dato::class, function (Faker $faker) {

    return [
        'nombre' => $faker->word,
        'telefono' => $faker->word,
        'productos' => $faker->word,
        'horario' => $faker->word,
        'imagen' => $faker->text,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s'),
        'deleted_at' => $faker->date('Y-m-d H:i:s')
    ];
});
