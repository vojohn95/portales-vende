<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Telefono;
use Faker\Generator as Faker;

$factory->define(Telefono::class, function (Faker $faker) {

    return [
        'id_data' => $faker->word,
        'telefono' => $faker->word,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s'),
        'deleted_at' => $faker->date('Y-m-d H:i:s')
    ];
});
