<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->rememberToken();
            $table->timestamps();
        });

        Schema::create('data', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nombre');
            $table->string('telefono');
            $table->string('productos');
            $table->string('horario');
            $table->longText('imagen')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('telefonos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('id_data');
            $table->foreign('id_data')
                ->references('id')
                ->on('data')
                ->onDelete('cascade');
            $table->string('telefono');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');

        Schema::dropIfExists('data');
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */

}
