<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name'=>'John Vo',
            'email'=> 'vojohn95@gmail.com',
            'password'=> Hash::make('Nemesis85'),
        ]);
    }
}
