@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Dato
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($dato, ['route' => ['datos.update', $dato->id], 'method' => 'patch']) !!}

                        @include('datos.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection