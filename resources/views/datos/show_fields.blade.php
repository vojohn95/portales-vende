<!-- Nombre Field -->
<div class="form-group">
    {!! Form::label('nombre', 'Nombre:') !!}
    <p>{{ $dato->nombre }}</p>
</div>

<!-- Telefono Field -->
<div class="form-group">
    {!! Form::label('telefono', 'Telefono:') !!}
    <p>{{ $dato->telefono }}</p>
</div>

<!-- Productos Field -->
<div class="form-group">
    {!! Form::label('productos', 'Productos:') !!}
    <p>{{ $dato->productos }}</p>
</div>

<!-- Horario Field -->
<div class="form-group">
    {!! Form::label('horario', 'Horario:') !!}
    <p>{{ $dato->horario }}</p>
</div>

<!-- Imagen Field -->
<div class="form-group">
    {!! Form::label('imagen', 'Imagen:') !!}
    <p>{{ $dato->imagen }}</p>
</div>

