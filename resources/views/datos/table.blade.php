<div class="table-responsive">
    <table class="table" id="datos-table">
        <thead>
            <tr>
                <th>Nombre</th>
        <th>Telefono</th>
        <th>Productos</th>
        <th>Horario</th>
        <th>Imagen</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($datos as $dato)
            <tr>
                <td>{{ $dato->nombre }}</td>
            <td>{{ $dato->telefono }}</td>
            <td>{{ $dato->productos }}</td>
            <td>{{ $dato->horario }}</td>
            <td>{{ $dato->imagen }}</td>
                <td>
                    {!! Form::open(['route' => ['datos.destroy', $dato->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('datos.show', [$dato->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                        <a href="{{ route('datos.edit', [$dato->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
