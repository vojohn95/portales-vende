<div class="form-row mb-4">
    <div class="col">
        <div class="md-form">
            <!-- First name -->
            {!! Form::label('nombre', 'Nombre:*') !!}
            {!! Form::text('nombre', null, ['class' => 'form-control', 'required']) !!}
        </div>
    </div>
    <div class="col">
        <div class="md-form">
            <!-- First name -->
            {!! Form::label('telefono', 'Telefono:*') !!}
            {!! Form::text('telefono', null, ['class' => 'form-control', 'required']) !!}
        </div>
    </div>
    <div class="col">
        <div class="md-form">
            <!-- First name -->
            {!! Form::label('productos', 'Productos:*') !!}
            {!! Form::text('productos', null, ['class' => 'form-control', 'required']) !!}
        </div>
    </div>
    <div class="col">
        <div class="md-form">
            <!-- First name -->
            {!! Form::label('horario', 'Horario:') !!}
            {!! Form::text('horario', null, ['class' => 'form-control']) !!}
        </div>
    </div>
</div>
