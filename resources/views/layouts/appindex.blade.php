<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Portales vende</title>
    <meta name="autor" content="Luis Fernando Jonathan Vargas Osornio - vojohn95@gmail.com">
    <meta name="autor" content="Esquitechs">
    <title>@yield('title' , 'Portales Vende')</title>
    <link rel="dns-prefetch" href="//fonts.gstatic.com" rel=preload>
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel=preload type="text/css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" rel=preload>
    <meta name="csrf-token" content="{{ csrf_token() }}"/>
</head>

<style>@media (max-width:450px){html,body,header{height:850px}}@media (min-width:451px) and (max-width:740px){html,body,header{height:900px}}@media (min-width:800px) and (max-width:850px){html,body,header{height:950px}}</style>


@include('layouts.navs.nav')

<body style="padding-top: 90px;">
<main class="py-4">
    <div class="container">

        @yield('content')

    </div>
</main>
</body>
<div style="padding-top: 90px;"></div>
<footer class="page-footer font-small black fixed-bottom">

    <!-- Copyright -->
    <div class="footer-copyright text-center py-3">© <?php echo date("Y"); ?>
        <a href="http://esquitechs.com/" class="font-weight-bold ml-1" target="_blank">Hecho con <i
                class="fa fa-heart" style="color:red"></i>
            por Esquitechs &nbsp;<img src="{{asset('logo/Imagen33.webp')}}" id="logo" class="logo"
                                      style="width: 2.5%;"></a>
    </div>
    <!-- Copyright -->

</footer>
<link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
<link href="{{ asset('css/mdb.min.css') }}" rel="stylesheet">

<style>
    @keyframes rotate {
        from {
            transform: rotate(0deg);
        }
        to {
            transform: rotate(360deg);
        }
    }

    @-webkit-keyframes rotate {
        from {
            -webkit-transform: rotate(0deg);
        }
        to {
            -webkit-transform: rotate(360deg);
        }
    }

    .logo {
        -webkit-animation: 3s rotate linear infinite;
        animation: 3s rotate linear infinite;
        -webkit-transform-origin: 50% 50%;
        transform-origin: 50% 50%;
    }
</style>

<link href="{{ asset('css/style.css') }}" rel="stylesheet">
<link rel="icon" href="{{asset('logo/Imagen33.webp')}}"/>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script>
    $('div.alert').not('.alert-important').delay(4000).fadeOut(1000);
</script>
@stack('maps')
<script type="text/javascript" src="{{asset('js/popper.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/bootstrap.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/mdb.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/moment.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/lazysizes.min.js')}}" async=""></script>

<script>
    @stack('scripts')
</script>


</body>
</html>
