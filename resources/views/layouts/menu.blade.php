<li class="{{ Request::is('datos*') ? 'active' : '' }}">
    <a href="{{ route('datos.index') }}"><i class="fa fa-edit"></i><span>Datos</span></a>
</li>

<li class="{{ Request::is('telefonos*') ? 'active' : '' }}">
    <a href="{{ route('telefonos.index') }}"><i class="fa fa-edit"></i><span>Telefonos</span></a>
</li>

