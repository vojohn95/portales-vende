
    <!--Navbar-->
    <nav class="navbar navbar-expand-lg navbar-dark fixed-top scrolling-navbar black">

        <!-- Navbar brand -->
        <a class="navbar-brand font-weight-bold" style="padding-left: 100px;" href="{{url("/")}}">PORTALES VENDE</a>

        <!-- Collapse button -->
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#basicExampleNav"
                aria-controls="basicExampleNav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <!-- Collapsible content -->
        <div class="collapse navbar-collapse" id="basicExampleNav">

            <!-- Links -->
            <ul class="navbar-nav mr-auto">
                <li class="nav-item">
                    <a class="nav-link" href="#formulario">Unirse</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#vendedores">Vendedores</a>
                </li>
                <!--<li class="nav-item">
                    <a class="nav-link" href="#">Pricing</a>
                </li>
                -->
                <!-- Dropdown -->
                <!--<li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" id="navbarDropdownMenuLink" data-toggle="dropdown"
                       aria-haspopup="true" aria-expanded="false">Dropdown</a>
                    <div class="dropdown-menu dropdown-primary" aria-labelledby="navbarDropdownMenuLink">
                        <a class="dropdown-item" href="#">Action</a>
                        <a class="dropdown-item" href="#">Another action</a>
                        <a class="dropdown-item" href="#">Something else here</a>
                    </div>
                </li>
            -->
            </ul>
            <!-- Links -->

            <ul class="navbar-nav ml-auto nav-flex-icons">
                <li class="nav-item">
                    <a class="btn-floating btn-sm btn-fb" type="button" target="_blank" href="https://www.facebook.com/esquitechs" role="button"><i class="fab fa-facebook-f"></i></a>

                </li>
                <li class="nav-item">
                    <a class="btn-floating btn-sm btn-git" type="button"  target="_blank" href="https://github.com/LfJohnVo" role="button"><i class="fab fa-github"></i></a>
                </li>
                <li class="nav-item">
                    <a class="btn-floating btn-sm btn-whatsapp success-color" href='https://wa.me/525562542170' target="_blank" type="button" role="button"><i class="fab fa-whatsapp"></i></a>
                </li>
                <!--<li class="nav-item">
                    <a type="button" class="btn btn-md btn-tw" target="_blank" href="https://esquitechs.com">Esquitechs</a>
                </li>-->
                <!--<li class="nav-item">
                    <button type="button" class="btn btn-md btn-default">Contactanos</button>
                </li>-->
            </ul>
        </div>
        <!-- Collapsible content -->

    </nav>
    <!--/.Navbar-->


