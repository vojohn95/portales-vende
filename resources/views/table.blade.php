<!-- Table with panel -->

<div class="card card-cascade narrower">

    <!--Card image-->
    <div
        class="view view-cascade gradient-card-header blue-gradient narrower py-2 mx-4 mb-3 d-flex justify-content-between align-items-center">

        <div>
            <!--<button type="button" class="btn btn-outline-white btn-rounded btn-sm px-2">
                <i class="fas fa-th-large mt-0"></i>
            </button>
            <button type="button" class="btn btn-outline-white btn-rounded btn-sm px-2">
                <i class="fas fa-columns mt-0"></i>
            </button>-->
        </div>

        <a href="" class="white-text mx-3" style="font-size: 32px;">Vendedores</a>

        <div>
            <!-- <button type="button" class="btn btn-outline-white btn-rounded btn-sm px-2">
                 <i class="fas fa-pencil-alt mt-0"></i>
             </button>
             <button type="button" class="btn btn-outline-white btn-rounded btn-sm px-2">
                 <i class="far fa-trash-alt mt-0"></i>
             </button>
             <button type="button" class="btn btn-outline-white btn-rounded btn-sm px-2">
                 <i class="fas fa-info-circle mt-0"></i>
             </button>-->
        </div>

    </div>
    <!--/Card image-->

    <div class="px-4">
        <div class="table-responsive">

            <div class="table-wrapper">
                <div class="form-group pull-right">
                    <input type="text" class="search form-control" placeholder="¿Qué estas buscando?">
                </div>
                <!--Table-->
                <table class="table table-hover mb-0 text-centerq">

                    <!--Table head-->
                    <thead>
                    <tr>
                        <th class="th-lg">
                            <a>#
                                <!--<i class="fas fa-sort ml-1"></i>-->
                            </a>
                        </th>
                        <th class="th-lg">
                            <a>Nombre
                               <!-- <i class="fas fa-sort ml-1"></i>-->
                            </a>
                        </th>
                        <th class="th-lg">
                            <a>Telefono
                              <!-- <i class="fas fa-sort ml-1"></i>-->
                            </a>
                        </th>
                        <th class="th-lg">
                            <a>Productos
                              <!--  <i class="fas fa-sort ml-1"></i>-->
                            </a>
                        </th>
                        <th class="th-lg">
                            <a>Horario
                              <!--  <i class="fas fa-sort ml-1"></i>-->
                            </a>
                        </th>
                    </tr>
                    <tr class="warning no-result">
                        <td colspan="14" style="color:red; font-size: 20px;"><i class="fa fa-warning"></i> No se
                            encontro
                            registro con la información ingresada
                        </td>
                    </tr>
                    </thead>
                    <!--Table head-->

                    <!--Table body-->
                    <tbody>
                    @foreach($datos as $dato)
                    <tr>
                        <td>{{$dato->id}}</td>
                        <td>{{$dato->nombre}}</td>
                        <td>{{$dato->telefono}}</td>
                        <td>{{$dato->productos}}</td>
                        <td>{{$dato->horario}}</td>
                    </tr>
                    @endforeach
                    </tbody>
                    <!--Table body-->
                </table>
                <!--Table-->
            </div>
        </div>
    </div>

</div>
<!-- Table with panel -->

