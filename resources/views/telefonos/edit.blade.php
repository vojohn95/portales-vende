@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1>
            Telefono
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($telefono, ['route' => ['telefonos.update', $telefono->id], 'method' => 'patch']) !!}

                        @include('telefonos.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection