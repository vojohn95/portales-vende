<!-- Id Data Field -->
<div class="form-group col-sm-6">
    {!! Form::label('id_data', 'Id Data:') !!}
    {!! Form::number('id_data', null, ['class' => 'form-control']) !!}
</div>

<!-- Telefono Field -->
<div class="form-group col-sm-6">
    {!! Form::label('telefono', 'Telefono:') !!}
    {!! Form::text('telefono', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('telefonos.index') }}" class="btn btn-default">Cancel</a>
</div>
