<div class="table-responsive">
    <table class="table" id="telefonos-table">
        <thead>
            <tr>
                <th>Id Data</th>
        <th>Telefono</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($telefonos as $telefono)
            <tr>
                <td>{{ $telefono->id_data }}</td>
            <td>{{ $telefono->telefono }}</td>
                <td>
                    {!! Form::open(['route' => ['telefonos.destroy', $telefono->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('telefonos.show', [$telefono->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                        <a href="{{ route('telefonos.edit', [$telefono->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
