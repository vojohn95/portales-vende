@extends('layouts.appindex')

@section('title', 'Inicio')

@section('content')
    <section id="formulario" class="mb-5 pb-4">
        <br><br>
        <div class="row justify-content-center">
            <!-- Card -->
            <div class="card card-cascade">

                <!-- Card image -->
                <div class="view view-cascade gradient-card-header blue-gradient">

                    <!-- Title -->
                    <h2 class="card-header-title mb-3">Unete al grupo de ventas</h2>

                </div>

                <!-- Card content -->
                <div class="card-body card-body-cascade text-center">

                    <!-- Text -->
                    {!! Form::open(['route' => 'AñadirNuevo']) !!}
                    <p class="card-text">@include('fields')</p>
                    {!! Form::submit('Enviar', ['class' => 'btn btn-primary', 'id' => 'btn-one']) !!}
                    {!! Form::close() !!}


                    @include('layouts.errors')
                    @include('flash::message')

                    <hr>

                    <p class="card-header-subtitle mb-0">Los campos con * son obligatorios</p>

                </div>

            </div>
            <!-- Card -->
        </div>
    </section>

    <hr>
    <section id="vendedores" class="mb-5 pb-4" style="padding-top: 10px;">
        <br><br>
        <div class="row justify-content-center py-4">
            <!-- Card -->
        @include('table')
        <!-- Card -->
        </div>
    </section>
@endsection()

<script>
    @push('scripts')
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $("#Enviar").click(function (e) {
        e.preventDefault();
        var nombre = $('#nombre').val();
        var telefono = $('#telefono').val();
        $.ajax({
            type: "post",
            url: "{{route('Nuevo')}}",
            data: data,
            success: function (msg) {
                alert("Se ha realizado el POST con exito " + msg);
            }
        });
    });

    /*$('#btn-one').click(function () {
        $('#btn-one').html('<span class="spinner-border spinner-border-sm mr-2" role="status" aria-hidden="true"></span>Cargando...').addClass('disabled');
    });*/
    @endpush
</script>
