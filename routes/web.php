<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','IndexController@index');
Route::post('/nuevo', 'IndexController@AjaxRequestNuevo')->name('Nuevo');
Route::post('/añadir','IndexController@AñadirNuevo')->name('AñadirNuevo');

Auth::Routes();
Route::group(['middleware' => 'auth'], function () {
    Route::get('/home', 'HomeController@index')->name('home');

    Route::get('/home', 'HomeController@index')->middleware('verified');

    Route::resource('datos', 'DatoController');

    Route::resource('telefonos', 'TelefonoController');
});
